﻿using IllusionPlugin;
using UnityEngine;

namespace AccountingLocomotion
{
    public class AccountingLocomotion : IEnhancedPlugin, IPlugin
    {
        public string[] Filter
        {
            get
            {
                return new string[] { "Accounting" };
            }
        }

        public string Name
        {
            get
            {
                return "AccountingLocomotion";
            }
        }

        public string Version
        {
            get
            {
                return "0.1";
            }
        }

        public void OnApplicationQuit()
        {
        }

        public void OnApplicationStart()
        {
            //get the left and right 'Hands'
            this.player = NewtonVR.NVRPlayer.Instance;
            this.leftHand = this.player.LeftHand;
            this.touchLeft = leftHand.Inputs[Valve.VR.EVRButtonId.k_EButton_Axis0];
            this.rightHand = this.player.LeftHand;
            this.touchRight = rightHand.Inputs[Valve.VR.EVRButtonId.k_EButton_Axis0];
        }

        public void OnLateUpdate()
        {
        }

        public void OnFixedUpdate()
        {
        }

        public void OnLevelWasInitialized(int level)
        {
        }

        public void OnLevelWasLoaded(int level)
        {
        }

        public void OnUpdate()
        {
            //check if player has been assinged
            if (this.player == null)
            {
                this.player = NewtonVR.NVRPlayer.Instance;
            }

            //check if left hand has been assigned
            if (this.leftHand == null)
            {
                this.leftHand = this.player.LeftHand;
                this.touchLeft = leftHand.Inputs[Valve.VR.EVRButtonId.k_EButton_Axis0];
            }

            //check if right hand has been assigned
            if (this.rightHand == null)
            {
                this.rightHand = this.player.RightHand;
                this.touchRight = this.rightHand.Inputs[Valve.VR.EVRButtonId.k_EButton_Axis0];
            }

            //get axis
            Vector2 touchLeftAxis = this.touchLeft.Axis;
            Vector2 touchRightAxis = this.touchRight.Axis;

            //remember anything they are holding
            NewtonVR.NVRInteractable currentlyInteracting = this.leftHand.CurrentlyInteracting;
            NewtonVR.NVRInteractable currentlyInteracting2 = this.rightHand.CurrentlyInteracting;

            //is the player trying to move?
            if (touchLeftAxis.x != 0 || touchLeftAxis.y != 0)
            {
                //set speed
                float tspeed = this.speed;

                //do they want to sprint?
                if (this.touchLeft.IsPressed)
                {
                    tspeed = tspeed * 2f;
                }

                //movement
                Vector3 Move = new Vector3(
                    this.player.Head.transform.forward.x * (touchLeftAxis.y * tspeed) +
                    this.player.Head.transform.right.x * (touchLeftAxis.x * tspeed),
                    0,
                    this.player.Head.transform.forward.z * (touchLeftAxis.y * tspeed) +
                    this.player.Head.transform.right.z * (touchLeftAxis.x * tspeed)
                );

                //move the player
                this.player.transform.position += Move;

                //move whatever they are holding with them
                if (currentlyInteracting != null)
                {
                    currentlyInteracting.transform.position += Move;
                }

                if (currentlyInteracting2 != null)
                {
                    currentlyInteracting2.transform.position += Move;
                }
            }

            //rotation, WIP
            /*if (touchRightAxis.y < -0.8f)
            {
                this.player.transform.RotateAround(this.player.Head.transform.position, new Vector3(0, 1, 0), 180);
            }
            else */if (touchRightAxis.x > 0.01f || touchRightAxis.x < -0.01f)
            {
                this.player.transform.RotateAround(this.player.Head.transform.position, new Vector3(0, 1, 0), touchRightAxis.x * (Time.deltaTime * this.rotationSpeed));
            }
        }

        public float speed = 0.025f;

        public float rotationSpeed = 80f;

        protected NewtonVR.NVRPlayer player;

        protected NewtonVR.NVRHand leftHand;

        protected NewtonVR.NVRButtonInputs touchLeft;

        protected NewtonVR.NVRHand rightHand;

        protected NewtonVR.NVRButtonInputs touchRight;
    }
}
