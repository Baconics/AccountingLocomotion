# Accounting Locomotion

Accounting Locomotion is a plugin that allows you to freely move and rotate your character in the VR game ["Accounting"](http://store.steampowered.com/app/518580/Accounting/) .

Accounting Locomotion (currently) does not handle collisions (you will noclip through everything) and only supports smooth locomotion (no teleportation).

## How To Install

1. Download and install Eusth's Illusion Plugin Architecture (https://github.com/Eusth/IPA/releases)
2. Download the [latest release](https://gitlab.com/Baconics/AccountingLocomotion/uploads/844f89cf715b52bf77872ad7a56e2eaa/AccountingLocomotion.dll)
3. Put AccountingLocomotion.dll in the plugins directory.

## How To Uninstall

1. Delete AccountingLocomotion.dll from the plugins directory.

## Controls

* Use the **left touchpad/thumbstick** to move, press down to 'sprint'.
* Use the **right touchpad/thumbstick** to rotate the camera.